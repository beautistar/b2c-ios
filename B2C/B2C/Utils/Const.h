//
//  Const.h
//  B2C
//
//  Created by Developer on 12/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Const : NSObject

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#pragma mark - COLOR
#define MAIN_COLOR                          [UIColor colorWithRed:64/255.0 green:0 blue:0 alpha:1.0]

// string const

#define SAVE_ROOT_PATH                      @"B2C"

#define CONN_ERROR                          @"Connect server fail.\n Please try again."

#define ALERT_TITLE                         @"B2C"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"
#define INPUT_EMAIL                         @"Please enter your Email address."
#define VALID_EMAIL                         @"Please enter valid Email address"
#define VALID_PWD                           @"Password must be at least 6 characters"
#define INPUT_USERFNAME                     @"Please enter your first name."
#define INPUT_USERLNAME                     @"Please enter your last name."
#define INPUT_PWD                           @"Please enter your password."

#define MSG_FEEDBACK                        @"You have successfully submitted your feedback.\nThanks for your precious time!"

#define MSG_UN_SERVICE                      @"Service unavailable \n\n This function is not ready to use"


#define EXIST_USERNAME                      @"User name already exist."
#define EXIST_EMAILADDRESS                  @"Email address already exist."
#define UNREGISTERED_EMAIL                  @"Unregistered email address."
#define WRONG_PASSWORD                      @"Wrong password."

#define INPUT_BUSINESSNAME                  @"Please enter business name."
#define INPUT_DESCRIPTION                   @"Please enter business description."
#define INPUT_CONTACTINFO                   @"Please enter contact info."
#define SELECT_CATEGORY                     @"Please select category."
#define SELECT_STATE                        @"Please select state."
#define SELECT_CITY                         @"Please select city."
#define INPUT_SPECIAL_OFFER                 @"Please enter special offer."
#define SELECT_LOGO                         @"Please select company logo."

#define INPUT_RECOMMEND                     @"Please enter recommendation."

#define UPDATE_FAIL                         @"Failed to create business."
#define SUCCESS_UPDATE                      @"Succeed to create business."
#define SUCCESS_EDIT                        @"Succeed to edit business."
#define SUCCESS_RECOMMEND                   @"Succeed to add recommendation."
#define SUCCESS_RESETPWD                    @"Succeed to reset password."
#define EXIST_BUSINESSNAME                  @"Bsuiness name already exist."
#define COMPLETE_BUSINESS                   @"Please complete your business profile."
#define CANT_RECOMMEND                      @"Business user can't add recommendation."

// Integer const

#define FROM_SHOP                           111
#define FROM_DISCOVER                       222
#define FROM_HOME                           333
//#define FROM_BUSINESS                       333

//#define FROM_LIST                           444
//#define FROM_RECOMMEND                      555
//
#define BUYER_USER                          11
#define SELLER_USER                         22

extern int addedRecommend;
extern int updatedBusiness;
@end
