//
//  HomeMenuCell.h
//  B2C
//
//  Created by JIS on 4/13/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol menuButtonDelegate <NSObject>

- (void) gotoCategory;
- (void) gotoMobileTopUp;
- (void) gotoSpecialOffer;
- (void) gotoDoBusiness;
- (void) gotoRecommend;

@end

@interface HomeMenuCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (weak, nonatomic) IBOutlet UIButton *btnTopUp;
@property (weak, nonatomic) IBOutlet UIButton *btnDoBusiness;
@property (weak, nonatomic) IBOutlet UIButton *btnRecommend;
@property (weak, nonatomic) IBOutlet UIButton *btnSpecialOffer;

@property (nonatomic, strong) id<menuButtonDelegate> delegate;

@end
