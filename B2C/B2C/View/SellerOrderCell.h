//
//  SellerOrderCell.h
//  B2C
//
//  Created by JIS on 4/24/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end
