//
//  HomeMenuCell.m
//  B2C
//
//  Created by JIS on 4/13/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "HomeMenuCell.h"

@implementation HomeMenuCell

- (IBAction)gotoCategory:(id)sender {
    
    [self gotoCategory];
}

- (IBAction)gotoTopUp:(id)sender {
    
    [self gotoTopUp];
}

- (IBAction)gotoDoBusiness:(id)sender {
    [self gotoDoBusiness];
}

- (IBAction)gotoRecommend:(id)sender {
    [self gotoRecommend];
}
- (IBAction)gotoSpecialOffer:(id)sender {
    [self gotoSpecialOffer];
}

- (void) gotoCategory {
    
    // go to profile page.
    if ([self.delegate respondsToSelector:@selector(gotoCategory)]) {
        [self.delegate gotoCategory];
    }
}

- (void) gotoTopUp {
    
    // go to profile page.
    if ([self.delegate respondsToSelector:@selector(gotoMobileTopUp)]) {
        [self.delegate gotoMobileTopUp];
    }
}

- (void) gotoSpecialOffer {
    
    // go to profile page.
    if ([self.delegate respondsToSelector:@selector(gotoSpecialOffer)]) {
        [self.delegate gotoSpecialOffer];
    }
}

- (void) gotoDoBusiness {
    
    // go to profile page.
    if ([self.delegate respondsToSelector:@selector(gotoDoBusiness)]) {
        [self.delegate gotoDoBusiness];
    }
}

- (void) gotoRecommend {
    
    // go to profile page.
    if ([self.delegate respondsToSelector:@selector(gotoRecommend)]) {
        [self.delegate gotoRecommend];
    }
}

@end
