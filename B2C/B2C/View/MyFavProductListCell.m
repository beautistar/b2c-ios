//
//  MyFavProductListCell.m
//  B2C
//
//  Created by JIS on 4/20/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MyFavProductListCell.h"
#import "KxMenu.h"
#import "Const.h"

@interface MyFavProductListCell() {
    
    
}

@end
@implementation MyFavProductListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_moreButton setBackgroundColor:UIColor.clearColor];
    [_moreButton addTarget:self
               action:@selector(moreAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [_moreButton setTitle:@"" forState:UIControlStateNormal];
    _moreButton.frame = CGRectMake(self.frame.size.width - 60 ,  self.frame.origin.y +10 , 50.0, 40.0);
    
    [self addSubview:_moreButton];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)moreAction:(UIButton *)sender {
    
    NSArray *menuItems =
    @[
      
      //[KxMenuItem menuItem:@"ACTION MENU 1234456"
      //               image:nil
      //              target:nil
      //              action:NULL],
      
      [KxMenuItem menuItem:@"Add to cart"
                     image:[UIImage imageNamed:@"menu_icon_shopcart"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Cancel favorite"
                     image:[UIImage imageNamed:@"menu_icon_favorite"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Go to shop"
                     image:[UIImage imageNamed:@"menu_icon_shop"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Share"
                     image:[UIImage imageNamed:@"menu_icon_share"]
                    target:self
                    action:@selector(pushMenuItem:)],
      ];
    
    for (int i=0;i<menuItems.count;i++){
        KxMenuItem *first = menuItems[i];
        first.foreColor = MAIN_COLOR;
        first.alignment = NSTextAlignmentLeft;
    }
    [KxMenu showMenuInView:self.superview
                  fromRect:CGRectMake(self.frame.size.width - 50 ,  self.frame.origin.y + 5 , 40.0, 30.0)
                 menuItems:menuItems];
}

- (void) pushMenuItem:(id)sender
{
    NSLog(@"%@", sender);
}

@end
