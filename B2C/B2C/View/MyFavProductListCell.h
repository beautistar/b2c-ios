//
//  MyFavProductListCell.h
//  B2C
//
//  Created by JIS on 4/20/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFavProductListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imvMoreButton;
@property (nonatomic, strong) UIButton *moreButton;
@end
