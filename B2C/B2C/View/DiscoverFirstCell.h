//
//  DiscoverFirstCell.h
//  B2C
//
//  Created by Developer on 12/21/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverFirstCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvCover;
@property (weak, nonatomic) IBOutlet UIImageView *imvProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;


@end
