//
//  CountryCell.h
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryEntity.h"

@interface CountryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDialCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryName;

- (void) setCell:(CountryEntity *) country;
@end
