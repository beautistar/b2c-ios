//
//  DiscoverListCell.m
//  B2C
//
//  Created by Developer on 12/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DiscoverListCell.h"

@implementation DiscoverListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
    
    _collectioViewHeight.constant = self.frame.size.width ;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
