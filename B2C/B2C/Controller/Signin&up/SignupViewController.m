//
//  SignupViewController.m
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SignupViewController.h"
#import "CountryEntity.h"
#import "CountryCell.h"

#import "CommonUtils.h"

@interface SignupViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *countryList;
    
    __weak IBOutlet UITableView *tblCountryList;
    __weak IBOutlet UITextField *tfName;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfDialCode;
    __weak IBOutlet UITextField *tfPhoneNumber;
    __weak IBOutlet UITextField *tfVerificationCode;
    __weak IBOutlet UITextField *tfCreatePassword;
    __weak IBOutlet UITextField *tfRetypePassword;
    __weak IBOutlet UIImageView *imvCheck;
    __weak IBOutlet UITextField *lblShopID;
    __weak IBOutlet UITextField *tfShopID;
    
    __weak IBOutlet NSLayoutConstraint *creatPwdY;
    __weak IBOutlet NSLayoutConstraint *creatPwdInputY;

    BOOL isChecked;
    
    
}

@end

@implementation SignupViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    isChecked = NO;
    
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setUserView];
}

- (void) initData {
    
    [tblCountryList setHidden:YES];
    
    tblCountryList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    countryList = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    NSError *error;
    NSString *fileContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        
        NSLog(@"Error reading file : %@", error.localizedDescription);
    }
    
    NSArray *dataList = (NSArray *)[NSJSONSerialization JSONObjectWithData:[fileContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
    
    for (int i = 0 ; i < dataList.count ; i++) {
        
        CountryEntity * _country = [[CountryEntity alloc] init];
        id keyValuePair = dataList[i];
        _country._countryName = keyValuePair[@"name"];
        _country._dialCode = keyValuePair[@"dial_code"];
        
        [countryList addObject:_country];
    }
    
    NSLog(@"Countrylist count : %d", (int)[countryList count]);
}

- (void) setUserView {
    
    NSLog(@"user type : %d", self._userType);
    
    if (self._userType == BUYER_USER) {
        
        creatPwdY.constant = 40;
        creatPwdInputY.constant = 40;
        [lblShopID setHidden:YES];
        [tfShopID setHidden:YES];
        
    } else {
        
        creatPwdY.constant = 80;
        creatPwdInputY.constant = 80;
        [lblShopID setHidden:NO];
        [tfShopID setHidden:NO];
        
    }
}

#pragma mark - country tableview datasource & delegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [countryList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryCell * cell = (CountryCell *) [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
    
    [cell setCell:countryList[indexPath.row]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryEntity * _country = countryList[indexPath.row];
    tfDialCode.text = _country._dialCode;
    
     [tblCountryList setHidden:YES];
}

- (IBAction)countryAction:(id)sender {
    
    [tblCountryList setHidden:NO];
    
}

- (IBAction)checkAction:(id)sender {
    
    if (isChecked) {
        [imvCheck setImage:[UIImage imageNamed:@"icon_checked_off"]];
        [tfCreatePassword setSecureTextEntry:YES];
        [tfRetypePassword setSecureTextEntry:YES];
    } else {
        [imvCheck setImage:[UIImage imageNamed:@"icon_checked_on"]];
        [tfCreatePassword setSecureTextEntry:NO];
        [tfRetypePassword setSecureTextEntry:NO];
    }
    
    isChecked = !isChecked;
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tfVerificationCode isFirstResponder] || [tfCreatePassword isFirstResponder] || [tfRetypePassword isFirstResponder]) {
        return 150;
    }
    
    return 200;
}
- (IBAction)signupAction:(id)sender {
    
    [self gotoMain];
}

- (void) gotoMain {
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabVC"]]; 
}

@end
