//
//  SigninViewController.m
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SigninViewController.h"
#import "SignupViewController.h"
#import "CommonUtils.h"

@interface SigninViewController () {
    
    __weak IBOutlet UITextField *tfId;
    __weak IBOutlet UITextField *tfPassword;
    __weak IBOutlet UIImageView *imvCheck;
    
    BOOL isChecked;
}

@end

@implementation SigninViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isChecked = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkAction:(id)sender {
    
    if (isChecked) {
        [imvCheck setImage:[UIImage imageNamed:@"icon_checked_off"]];
        [tfPassword setSecureTextEntry:YES];
    } else {
        [imvCheck setImage:[UIImage imageNamed:@"icon_checked_on"]];
        [tfPassword setSecureTextEntry:NO];
    }
    
    isChecked = !isChecked;
}

- (IBAction)signinAction:(id)sender {
    
    if ([tfId.text isEqualToString:@"seller"]) {
        
        [self gotoMainSeller];
        
    } else {
        [self gotoMainBuyer];
        
    }
}

- (void) gotoMainBuyer {
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabVC"]];    
}

- (void) gotoMainSeller {
    
    UIStoryboard *sellerSB = [UIStoryboard storyboardWithName:@"Seller" bundle:nil];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[sellerSB instantiateViewControllerWithIdentifier:@"MainTabVC"]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    SignupViewController * signupVC = segue.destinationViewController;
    
    
    if ([segue.identifier isEqualToString:@"segueForSeller"]) {
        
        signupVC._userType = SELLER_USER;
    } else {
        signupVC._userType = BUYER_USER;
    }
}

@end
