//
//  CheckOutViewController.m
//  B2C
//
//  Created by Developer on 12/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CheckOutViewController.h"

@interface CheckOutViewController () {
    
    __weak IBOutlet UILabel *lblCount;
    
    int _qty;
    
}

@end

@implementation CheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabBarController.tabBar.hidden = YES;
    
    _qty = 2;
    lblCount.text = [NSString stringWithFormat:@"%d", _qty];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)plusAction:(id)sender {
    
    _qty++;
    lblCount.text = [NSString stringWithFormat:@"%d", _qty];
    
}

- (IBAction)minAction:(id)sender {
    
    if (_qty > 0) {
        _qty--;
        lblCount.text = [NSString stringWithFormat:@"%d", _qty];
    }
}

@end
