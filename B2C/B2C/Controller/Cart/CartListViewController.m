//
//  CartListViewController.m
//  B2C
//
//  Created by Developer on 12/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CartListViewController.h"
#import "CartListCell.h"

@interface CartListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblCartList;
    __weak IBOutlet UIImageView *imvTotalCheck;
    
    
    BOOL isTotal;
}

@end

@implementation CartListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tblCartList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    isTotal = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;

}
- (IBAction)chatAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:2];
}

- (IBAction)totalAction:(id)sender {
    
    if (isTotal) {
        [imvTotalCheck setImage:[UIImage imageNamed:@"icon_checked_on"]];
    } else {
        [imvTotalCheck setImage:[UIImage imageNamed:@"icon_checked_off"]];
    }
    
    isTotal = !isTotal;
}


#pragma mark - tableView delegate & DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 135.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CartListCell * cell = (CartListCell *) [tableView dequeueReusableCellWithIdentifier:@"CartListCell"];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        //[self.dataArray removeObjectAtIndex:indexPath.row];
        [tblCartList reloadData]; // tell table to refresh now
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
