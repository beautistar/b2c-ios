//
//  ShopAddressViewController.m
//  B2C
//
//  Created by Developer on 12/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ShopAddressViewController.h"

@interface ShopAddressViewController ()

@end

@implementation ShopAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
