//
//  MyFavProductListViewController.m
//  B2C
//
//  Created by JIS on 4/20/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MyFavProductListViewController.h"
#import "MyFavProductListCell.h"

@interface MyFavProductListViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation MyFavProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 6;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyFavProductListCell * cell = (MyFavProductListCell *) [tableView dequeueReusableCellWithIdentifier:@"MyFavProductListCell"];
    
    return cell;
}

@end
