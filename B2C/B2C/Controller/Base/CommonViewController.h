//
//  CommonViewController.h
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BaseViewController.h"

@interface CommonViewController : BaseViewController

- (CGFloat) getOffsetYWhenShowKeybarod;
- (void) registerNotification;
- (void) unregisterNotification;

@end
