//
//  AllProductViewController.m
//  B2C
//
//  Created by Developer on 12/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "AllProductViewController.h"
#import "ProductDetailViewController.h"
#import "ProductPhotoCell.h"

#import "CommonUtils.h"

@interface AllProductViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    
}

@end

@implementation AllProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
}

- (IBAction)gotoChatAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:2];
}
#pragma mark - 
#pragma mark - collectionview delegate & datasource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 9;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float cellWitdth = self.view.frame.size.width / 2.0 - 3;
    return CGSizeMake(cellWitdth, cellWitdth);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductPhotoCell *cell = (ProductPhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProductPhotoCell" forIndexPath:indexPath];
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductDetailViewController *detailVC = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
    
    detailVC._from = FROM_SHOP;
    
    [self.navigationController pushViewController:detailVC animated:YES];    
    
}

@end
