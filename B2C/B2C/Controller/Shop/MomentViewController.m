//
//  MomentViewController.m
//  B2C
//
//  Created by Developer on 12/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "MomentViewController.h"
#import "ProductDetailViewController.h"
#import "DiscoverListCell.h"
#import "DiscoverPhotoCell.h"



@interface MomentViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    
    __weak IBOutlet UITableView *tblMomentsList;
}

@end

@implementation MomentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - chatlist tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 225.0;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DiscoverListCell *cell = (DiscoverListCell *) [tableView dequeueReusableCellWithIdentifier:@"DiscoverListCell"];
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    ProductDetailViewController *detailVC = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
//    
//    detailVC._from = FROM_DISCOVER;
//    
//    [self.navigationController pushViewController:detailVC animated:YES];
    
    
}

#pragma mark - collection view datasource & delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DiscoverPhotoCell *photoCell = (DiscoverPhotoCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"DiscoverPhotoCell" forIndexPath:indexPath];
    
    return photoCell;
}


@end
