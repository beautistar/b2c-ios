//
//  ShopDetailViewController.m
//  B2C
//
//  Created by Developer on 12/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ShopDetailViewController.h"
#import "CommonUtils.h"

@interface ShopDetailViewController () {
    
    BOOL isFollowed;
    __weak IBOutlet UIButton *btnFollow;
}

@end

@implementation ShopDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isFollowed = YES;
    [self setFollow:isFollowed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)followAction:(id)sender {
    
    isFollowed = !isFollowed;
    [self setFollow:isFollowed];
}

- (void) setFollow:(BOOL) isFollow {
    
    if (isFollow) {
        
        btnFollow.backgroundColor = [UIColor lightGrayColor];
        [btnFollow setTitle:@"Unfollow" forState:UIControlStateNormal];
    } else {
        btnFollow.backgroundColor = MAIN_COLOR;
        [btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
