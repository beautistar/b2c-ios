//
//  AddProductViewController.m
//  B2C
//
//  Created by Developer on 12/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "AddProductViewController.h"
#import "ActionSheetPicker.h"
#import "UITextView+Placeholder.h"

@interface AddProductViewController () <UIScrollViewDelegate> {
    
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *tfSize;
    __weak IBOutlet UITextField *tfColor;
    __weak IBOutlet UIButton *btnSize;
    __weak IBOutlet UIButton *btnColor;
    __weak IBOutlet UITextView *tvDescription;
    __weak IBOutlet UITextField *tfCategory;
    __weak IBOutlet UIButton *btnCategory;
    __weak IBOutlet UIImageView *imvInStock;
    __weak IBOutlet UIImageView *imvPreOrder;
    
    NSArray *_sizes;
    NSArray *_colors;
    NSArray *_categories;
    CGFloat _scrollViewHeight;
    
    BOOL inStockChecked;
    
}

@end

@implementation AddProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _sizes = [[NSArray alloc] initWithObjects:@"M", @"L", @"XL", @"XXL", @"XXXL", nil];
    
    _colors = [[NSArray alloc] initWithObjects:@"Red", @"Black", @"Brown", @"White", @"Blue", @"Yellow", nil];
    _categories = [[NSArray alloc] initWithObjects:@"Category - 1", @"Category - 2", @"Category - 3", @"Category - 4", @"Category - 5", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    tvDescription.placeholder = @"Please enter product detail";
    
    _scrollViewHeight = scrollView.frame.size.height;
    
    NSLog(@"%f", _scrollViewHeight);
    
    inStockChecked = YES;
    

    [self initView];
}

- (void) initView {
    
    int imageCount = 6;
    
    // Scroll View
    
    scrollView.backgroundColor=[UIColor clearColor];
    scrollView.delegate=self;
    scrollView.pagingEnabled=YES;
    scrollView.alwaysBounceVertical = NO;
    scrollView.bounces = NO;
    
    [scrollView setContentSize:CGSizeMake((_scrollViewHeight+5)*imageCount, _scrollViewHeight)];
    
//    // page control
//    
//    pageControl.backgroundColor=[UIColor clearColor];
//    pageControl.numberOfPages=imageCount;
//    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    for(int i=1;i<=imageCount;i++)
    {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, _scrollViewHeight, _scrollViewHeight)];
        [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"image%d",i%imageCount]]];
        [scrollView addSubview:image];
        
        x+=_scrollViewHeight;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    //CGFloat viewWidth = _scrollView.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
//    int pageNumber = floor((_scrollView.contentOffset.x - viewWidth/50) / viewWidth) +1;
//    
//    pageControl.currentPage=pageNumber;
    
}

//- (void)pageChanged {

//    int pageNumber = (int)pageControl.currentPage;
//    
//    CGRect frame = scroll.frame;
//    frame.origin.x = frame.size.width*pageNumber;
//    frame.origin.y=0;
//    
//    [scroll scrollRectToVisible:frame animated:YES];
//}

- (IBAction)sizeAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Size" rows:_sizes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfSize setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnSize];
}

- (IBAction)colorAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Color" rows:_colors initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfColor setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnColor];
}

- (IBAction)categoryAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:_categories initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfCategory setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnCategory];
    
    
}

- (IBAction)inStockAction:(id)sender {
    
    imvInStock.image = [UIImage imageNamed:@"icon_checked_on"];
    imvPreOrder.image = [UIImage imageNamed:@"icon_checked_off"];
    
}

- (IBAction)preOrderAction:(id)sender {

    
    imvInStock.image = [UIImage imageNamed:@"icon_checked_off"];
    imvPreOrder.image = [UIImage imageNamed:@"icon_checked_on"];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
