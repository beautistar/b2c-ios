//
//  MyFavoriteViewController.m
//  B2C
//
//  Created by JIS on 4/19/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MyFavoriteViewController.h"
#import "MyFavProductListViewController.h"
#import "MyFavShopListViewController.h"
#import <CarbonKit/CarbonKit.h>
#import "CommonUtils.h"

@interface MyFavoriteViewController () <CarbonTabSwipeNavigationDelegate> {
    
    NSArray *items;
    CarbonTabSwipeNavigation * carbonTabSwipeNavigation;
}

@end

@implementation MyFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    items = @[[UIImage imageNamed:@"icon_product_image"], [UIImage imageNamed:@"icon_shop_image"]];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:self.view];
    
    [self style];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)style {
    
    //    UIColor *color = [UIColor colorWithRed:24.0 / 255 green:75.0 / 255 blue:152.0 / 255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = MAIN_COLOR;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbarHeight.constant = 60.0;
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:MAIN_COLOR];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/2.0 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/2.0 forSegmentAtIndex:1];
    //[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.0 forSegmentAtIndex:2];
    //[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/4.0 forSegmentAtIndex:3];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[MAIN_COLOR colorWithAlphaComponent:0.1]
                                        font:[UIFont boldSystemFontOfSize:12]];
    [carbonTabSwipeNavigation setSelectedColor:MAIN_COLOR font:[UIFont boldSystemFontOfSize:12]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"MyFavProductListViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"MyFavShopListViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    //    switch (index) {
    //        case 0:
    //            self.title = @"Home";
    //            break;
    //        case 1:
    //            self.title = @"Hourglass";
    //            break;
    //        case 2:
    //            self.title = @"Premium Badge";
    //            break;
    //        default:
    //            self.title = items[index];
    //            break;
    //    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %d", (int)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}



- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
