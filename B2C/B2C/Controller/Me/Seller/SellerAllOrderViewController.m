//
//  SellerAllOrderViewController.m
//  B2C
//
//  Created by JIS on 4/23/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "SellerAllOrderViewController.h"
#import "SellerOrderCell.h"

@interface SellerAllOrderViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation SellerAllOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView 

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SellerOrderCell * cell = (SellerOrderCell *) [tableView dequeueReusableCellWithIdentifier:@"SellerOrderCell"];
    
    cell.lblStatus.text = @"Delivered";
    
    return cell;
}

@end
