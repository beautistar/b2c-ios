//
//  SellerMyOrderHomeViewController.m
//  B2C
//
//  Created by JIS on 4/23/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "SellerMyOrderHomeViewController.h"
#import <CarbonKit/CarbonKit.h>
#import "CommonUtils.h"

@interface SellerMyOrderHomeViewController () <CarbonTabSwipeNavigationDelegate> {
    
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    
    __weak IBOutlet UIView *contentView;
}

@end

@implementation SellerMyOrderHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabBarController.tabBar.hidden = YES;

    items = @[[UIImage imageNamed:@"btn_allorder_select"],
              [UIImage imageNamed:@"btn_paid_select"],
              [UIImage imageNamed:@"btn_delivery_select"],
              [UIImage imageNamed:@"btn_delivered_select"],
              [UIImage imageNamed:@"btn_check_select"]];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self andTargetView:contentView];
    
    [self style];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)style {
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = MAIN_COLOR;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbarHeight.constant = 40;
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:MAIN_COLOR];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/5.0 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/5.0 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/5.0 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/5.0 forSegmentAtIndex:3];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/5.0 forSegmentAtIndex:4];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[MAIN_COLOR colorWithAlphaComponent:0.1]
                                        font:[UIFont boldSystemFontOfSize:12]];
    [carbonTabSwipeNavigation setSelectedColor:MAIN_COLOR font:[UIFont boldSystemFontOfSize:12]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"SellerAllOrderViewController"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"SellerAllOrderViewController"];
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"SellerAllOrderViewController"];
        case 3:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"SellerAllOrderViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"SellerAllOrderViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    //    switch (index) {
    //        case 0:
    //            self.title = @"Home";
    //            break;
    //        case 1:
    //            self.title = @"Hourglass";
    //            break;
    //        case 2:
    //            self.title = @"Premium Badge";
    //            break;
    //        default:
    //            self.title = items[index];
    //            break;
    //    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %d", (int)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}


- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
