//
//  MyFavShopListViewController.m
//  B2C
//
//  Created by JIS on 4/20/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MyFavShopListViewController.h"
#import "MyFavShopListCell.h"

@interface MyFavShopListViewController ()

@end

@implementation MyFavShopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 5;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyFavShopListCell * cell = (MyFavShopListCell *) [tableView dequeueReusableCellWithIdentifier:@"MyFavShopListCell"];
    
    return cell;
}
@end
