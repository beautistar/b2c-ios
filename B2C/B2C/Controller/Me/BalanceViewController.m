//
//  BalanceViewController.m
//  B2C
//
//  Created by JIS on 4/21/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "BalanceViewController.h"
#import "Const.h"

@interface BalanceViewController ()

@end

@implementation BalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)topupAction:(id)sender {
    
    [self showAlertDialog:ALERT_TITLE message:MSG_UN_SERVICE positive:ALERT_OK negative:nil];
}

@end
