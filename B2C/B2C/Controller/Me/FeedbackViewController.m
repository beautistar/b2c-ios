//
//  FeedbackViewController.m
//  B2C
//
//  Created by JIS on 4/19/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "FeedbackViewController.h"
#import "Const.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)feedbackAction:(id)sender {
    
    [self showAlertDialog:ALERT_TITLE message:MSG_FEEDBACK positive:ALERT_OK negative:nil];
    
}

@end
