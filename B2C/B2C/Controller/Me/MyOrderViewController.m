//
//  MyOrderViewController.m
//  B2C
//
//  Created by JIS on 4/19/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "MyOrderViewController.h"

#import "MyOrderCell.h"

@interface MyOrderViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation MyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 140.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyOrderCell *cell = (MyOrderCell *) [tableView dequeueReusableCellWithIdentifier:@"MyOrderCell"];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

@end
