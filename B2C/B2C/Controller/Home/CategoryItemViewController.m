//
//  CategoryItemViewController.m
//  B2C
//
//  Created by JIS on 4/17/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "CategoryItemViewController.h"
#import "DiscoverPhotoCell.h"

@interface CategoryItemViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {

    BOOL isHidden;
    BOOL isLowToHighChecked;
    BOOL isHighToLowChecked;
    BOOL isCustomerReviewChecked;
    BOOL isNewestArrialsChecked;
    BOOL isFreedeliveryChecked;
    BOOL isInStockChecked;
    BOOL isPreOrderChecked;
    
    __weak IBOutlet UIImageView *imvLowTohigh;
    __weak IBOutlet UIImageView *imvHighToLow;
    __weak IBOutlet UIImageView *imvCustomerReview;
    __weak IBOutlet UIImageView *imvNewestArrivals;
    __weak IBOutlet UIImageView *imvFreeDelivery;
    __weak IBOutlet UIImageView *imvInStock;
    __weak IBOutlet UIImageView *imvPreOrder;
    
}

@property (weak, nonatomic) IBOutlet UIView *filterView;

@end

@implementation CategoryItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initData];
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    
    isHidden = YES;
    isLowToHighChecked = NO;
    isHighToLowChecked = NO;
    isCustomerReviewChecked = NO;
    isNewestArrialsChecked = NO;
    isFreedeliveryChecked = NO;
    isInStockChecked = NO;
    isPreOrderChecked = NO;
    
}

- (void) initView {
    
    [_filterView setHidden:YES];
    
}

- (IBAction)lowToHighAction:(id)sender {
    
    if (isLowToHighChecked) {
        [imvLowTohigh setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvLowTohigh setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isLowToHighChecked = !isLowToHighChecked;
}

- (IBAction)highToLowAction:(id)sender {
    
    if (isHighToLowChecked) {
        [imvHighToLow setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvHighToLow setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isHighToLowChecked = !isHighToLowChecked;
}
- (IBAction)customReviewAction:(id)sender {
    
    if (isCustomerReviewChecked) {
        [imvCustomerReview setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvCustomerReview setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isCustomerReviewChecked = !isCustomerReviewChecked;
}
- (IBAction)newestArrivalAction:(id)sender {
    
    if (isNewestArrialsChecked) {
        [imvNewestArrivals setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvNewestArrivals setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isNewestArrialsChecked = !isNewestArrialsChecked;
}
- (IBAction)freeDeliveryAction:(id)sender {
    
    if (isFreedeliveryChecked) {
        [imvFreeDelivery setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvFreeDelivery setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isFreedeliveryChecked = !isFreedeliveryChecked;
}
- (IBAction)inStockAction:(id)sender {
    
    if (isInStockChecked) {
        [imvInStock setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvInStock setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isInStockChecked = !isInStockChecked;
}
- (IBAction)preOrderAction:(id)sender {
    
    if (isPreOrderChecked) {
        [imvPreOrder setImage:[UIImage imageNamed:@"icon_checked_off"]];
    } else {
        [imvPreOrder setImage:[UIImage imageNamed:@"icon_checked_on"]];
    }
    
    isPreOrderChecked = !isPreOrderChecked;
}

- (IBAction)filterAction:(id)sender {
    
    if (isHidden) {
        [_filterView setHidden:NO];
    } else {
        [_filterView setHidden:YES];
    }
    
    isHidden = !isHidden;
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - collectionview

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DiscoverPhotoCell *cell = (DiscoverPhotoCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"DiscoverPhotoCell" forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.view.frame.size.width / 2.0 - 2, self.view.frame.size.width / 2.0 - 2);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2;
}
@end
