//
//  HomeViewController.m
//  B2C
//
//  Created by Developer on 12/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "HomeViewController.h"
#import "MobileTopupViewController.h"
#import "CategoryViewController.h"
#import "ProductDetailViewController.h"
#import "ChatListViewController.h"

#import "SDCycleScrollView.h"
#import "DiscoverPhotoCell.h"
#import "AdScrollCell.h"
#import "HomeMenuCell.h"

#import "Const.h"



@interface HomeViewController () <SDCycleScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, menuButtonDelegate> {
    
    
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.hidesBottomBarWhenPushed = NO;
    self.tabBarController.tabBar.hidden = NO;

    
    //[self initView];
}

- (void) initView {
    
    // 情景一：采用本地图片实现
    NSArray *imageNames = @[@"image1",
                            @"image2",
                            @"image3",
                            @"image2",
                            @"image1" // 本地图片请填写全名
                            ];
    
    // 情景二：采用网络图片实现
//    NSArray *imagesURLStrings = @[
//                                  @"https://ss2.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=a4b3d7085dee3d6d2293d48b252b5910/0e2442a7d933c89524cd5cd4d51373f0830200ea.jpg",
//                                  @"https://ss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/super/whfpf%3D425%2C260%2C50/sign=a41eb338dd33c895a62bcb3bb72e47c2/5fdf8db1cb134954a2192ccb524e9258d1094a1e.jpg",
//                                  @"http://c.hiphotos.baidu.com/image/w%3D400/sign=c2318ff84334970a4773112fa5c8d1c0/b7fd5266d0160924c1fae5ccd60735fae7cd340d.jpg"
//                                  ];
    
    CGFloat w = self.view.bounds.size.width;
    
    // 网络加载 --- 创建带标题的图片轮播器
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, -10, w, self.view.frame.size.height + 10) shouldInfiniteLoop:YES imageNamesGroup:imageNames];
//    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    [self.view addSubview:cycleScrollView];
    //cycleScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    //SDCycleScrollView *cycleScrollView2 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 280, w, 180) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
//    cycleScrollView2.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
//    cycleScrollView2.titlesGroup = titles;
//    cycleScrollView2.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
//    [adScrolContentView addSubview:cycleScrollView2];
    
    //         --- 模拟加载延迟
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        cycleScrollView2.imageURLStringsGroup = imagesURLStrings;
//    });

    
    
}

#pragma mark - CollectionView delegate & datasource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 20;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float cellWidth, cellHeight;
    
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        
        cellWidth = self.view.frame.size.width;
        cellHeight = 150;
    }
    
    else if ((indexPath.row-2) % 5 == 0 || (indexPath.row-2) % 5 == 1) {
        
        cellWidth = self.view.frame.size.width / 2.0 - 3;
        cellHeight = self.view.frame.size.width / 2.0 - 3;
    } else {
        cellWidth = self.view.frame.size.width / 3.0 - 4;
        cellHeight = self.view.frame.size.width / 2.0 - 3;
    }
    
    return CGSizeMake(cellWidth, cellHeight);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        AdScrollCell *adCell = (AdScrollCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"AdScrollCell" forIndexPath:indexPath];
        NSArray *imageNames = @[@"image1",
                                @"image2",
                                @"image3",
                                @"image2",
                                @"image1" // 本地图片请填写全名
                                ];
        CGFloat w = self.view.bounds.size.width;
        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, w, 150) shouldInfiniteLoop:YES imageNamesGroup:imageNames];
        cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;

        [adCell addSubview:cycleScrollView];
        
        return adCell;
        
    } else if (indexPath.row == 1) {
        
        HomeMenuCell * menuCell = (HomeMenuCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeMenuCell" forIndexPath:indexPath];
        menuCell.delegate = self;
        return menuCell;
    }
    else {
    
        DiscoverPhotoCell *cell = (DiscoverPhotoCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"DiscoverPhotoCell" forIndexPath:indexPath];
        
        return cell;
    }
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row > 1) {
        
        ProductDetailViewController * pDetailVC = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
        pDetailVC._from = FROM_HOME;
        [self.navigationController pushViewController:pDetailVC animated:YES];
        
    }
}

- (IBAction)chatAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:2];
    
    //NSMutableArray* newViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    //ChatListViewController *chatListVC = (ChatListViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChatListViewController"];
    /*
    
    //chatListVC.hidesBottomBarWhenPushed = YES;
    [newViewControllers replaceObjectAtIndex:newViewControllers.count-1 withObject:chatListVC];
    
    //set the new view Controllers in the navigationController Stack
    [self.navigationController setViewControllers:newViewControllers animated:YES];
     */
    //[self.navigationController pushViewController:chatListVC animated:NO];
}

- (void) gotoCategory {
    
    NSLog(@"goto category");
//    id categoryVC = (CategoryViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryViewController"];
//    [self.navigationController pushViewController:categoryVC animated:YES];
}

- (void) gotoMobileTopUp {
    
    NSLog(@"goto mobile topup");
    id mobileTopUpVC = (MobileTopupViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MobileTopupViewController"];
    [self.navigationController pushViewController:mobileTopUpVC animated:YES];
   
}

- (void) gotoSpecialOffer {
    
    NSLog(@"goto gotoSpecialOffer");
    
}

- (void) gotoDoBusiness {
    
    NSLog(@"goto gotoDoBusiness");
    
}

- (void) gotoRecommend {
    
    NSLog(@"goto gotoRecommend");
    
}



@end
