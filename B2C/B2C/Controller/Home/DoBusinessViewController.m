//
//  DoBusinessViewController.m
//  B2C
//
//  Created by JIS on 4/17/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "DoBusinessViewController.h"
#import "UITextView+Placeholder.h"

@interface DoBusinessViewController () {
    
    __weak IBOutlet UITextView *qTextView;
    
}

@end

@implementation DoBusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    qTextView.placeholder = @"Enter question here";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
