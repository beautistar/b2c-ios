//
//  ChatListViewController.m
//  B2C
//
//  Created by Developer on 12/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChattingViewController.h"
#import "ChatListCell.h"

@interface ChatListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblChatList;
}

@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.hidesBottomBarWhenPushed = NO;
    self.tabBarController.tabBar.hidden = NO;
    
    [self.tabBarController setSelectedIndex:2];
}

- (void) initView {    
    
    tblChatList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - chatlist tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80.0;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChatListCell *cell = (ChatListCell *) [tableView dequeueReusableCellWithIdentifier:@"ChatListCell"];
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self gotoChattingVC];
}

- (void) gotoChattingVC {
    
    ChattingViewController *chattingVC = (ChattingViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ChattingViewController"];
    
    [self.navigationController pushViewController:chattingVC animated:YES];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
