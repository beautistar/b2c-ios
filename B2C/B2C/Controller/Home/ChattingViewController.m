//
//  ChattingViewController.m
//  B2C
//
//  Created by Developer on 12/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ChattingViewController.h"

@interface ChattingViewController () {
    
    BOOL isAttachViewOpened;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachViewYConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageViewBottomConstrain;
@property (weak, nonatomic) IBOutlet UIView *messageView;

@end

@implementation ChattingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isAttachViewOpened = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.hidesBottomBarWhenPushed = YES;
    
    self.tabBarController.tabBar.hidden = YES;
    
    _messageViewBottomConstrain.constant = 0;
}

- (void) viewDidAppear:(BOOL)animated {

    _messageViewBottomConstrain.constant = 0;
    
}

- (IBAction)addAction:(id)sender {
    
    //NSLog(@"bottom Y : %f", (float) _messageViewBottomConstrain.constant);
    
    if (isAttachViewOpened) {
        
        _messageViewBottomConstrain.constant -= 80;        
        
    } else {
        
        _messageViewBottomConstrain.constant += 80;
    }
    
    isAttachViewOpened = !isAttachViewOpened;
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    return 0;
}

@end
