//
//  DiscoverViewController.m
//  B2C
//
//  Created by Developer on 12/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DiscoverViewController.h"
#import "ShopHomeViewController.h"
#import "ProductDetailViewController.h"
#import "DiscoverListCell.h"
#import "DiscoverFirstCell.h"
#import "DiscoverPhotoCell.h"

#import "CommonUtils.h"


@interface DiscoverViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    
    __weak IBOutlet NSLayoutConstraint *collectionViewHeight;
    __weak IBOutlet UIImageView *imvCover;
    __weak IBOutlet UIImageView *imvProfile;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UITableView *tblDiscoverList;
}

@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tblDiscoverList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    
    collectionViewHeight.constant = self.view.frame.size.width / 2.0 + 20;
}
- (IBAction)chatAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:2];
}

#pragma mark - chatlist tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        return 230;
    }
    
    return 60 + self.view.frame.size.width  + 70;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        DiscoverFirstCell *firstCell = (DiscoverFirstCell *) [tableView dequeueReusableCellWithIdentifier:@"DiscoverFirstCell"];
        
        return firstCell;
        
    } else {
    
        DiscoverListCell *cell = (DiscoverListCell *) [tableView dequeueReusableCellWithIdentifier:@"DiscoverListCell"];
        
        return cell;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShopHomeViewController *shopVC = (ShopHomeViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ShopHomeViewController"];
    
    [self.navigationController pushViewController:shopVC animated:YES];
}

#pragma mark - collection view datasource & delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DiscoverPhotoCell *photoCell = (DiscoverPhotoCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"DiscoverPhotoCell" forIndexPath:indexPath];
    
    return photoCell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductDetailViewController *detailVC = (ProductDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
    
    detailVC._from = FROM_DISCOVER;
    
    [self.navigationController pushViewController:detailVC animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.view.frame.size.width / 2.0 - 10.0, self.view.frame.size.width / 2.0 - 10.0);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2.0;
}

@end
