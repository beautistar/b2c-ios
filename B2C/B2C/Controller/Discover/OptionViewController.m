//
//  OptionViewController.m
//  B2C
//
//  Created by Developer on 12/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "OptionViewController.h"

@interface OptionViewController () {
    
    __weak IBOutlet UILabel *lblQty;
    __weak IBOutlet UIButton *btnSize;
    __weak IBOutlet UIButton *btnColor;
    
    int qty;
    NSString *_size;
    NSString *_color;
}

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    qty = 5;
    lblQty.text = [NSString stringWithFormat:@"%d", qty];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sizeAction:(id)sender {
    
    UIButton *tappedBtn = (UIButton *) sender;
    
    NSLog(@"%d", (int)tappedBtn.tag);
    
    _size = tappedBtn.titleLabel.text;
    
    for (int i = 11; i < 17; i++) {
        
        UIButton *sizeBtns = (UIButton *) [self.view viewWithTag:i];
        
        if (sizeBtns.tag == tappedBtn.tag) {
            
            sizeBtns.backgroundColor = [UIColor colorWithRed:64/255.0 green:0 blue:0 alpha:1.0];
        } else {
            sizeBtns.backgroundColor = [UIColor lightGrayColor];
        }
    }
}

- (IBAction)colorAction:(id)sender {
    
    UIButton *tappedBtn = (UIButton *) sender;
    
    NSLog(@"%d", (int)tappedBtn.tag);
    
    _color = tappedBtn.titleLabel.text;
    
    for (int i = 21; i < 27; i++) {
        
        UIButton *sizeBtns = (UIButton *) [self.view viewWithTag:i];
        
        if (sizeBtns.tag == tappedBtn.tag) {
            
            sizeBtns.backgroundColor = [UIColor colorWithRed:64/255.0 green:0 blue:0 alpha:1.0];
        } else {
            sizeBtns.backgroundColor = [UIColor lightGrayColor];
        }
    }
}

- (IBAction)nimAction:(id)sender {
    
    if (qty > 0) {
        qty--;
        lblQty.text = [NSString stringWithFormat:@"%d", qty];
    }
}

- (IBAction)plusAction:(id)sender {
    
    qty++;
    lblQty.text = [NSString stringWithFormat:@"%d", qty];
}

@end
