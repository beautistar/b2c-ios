//
//  ProductDetailViewController.m
//  B2C
//
//  Created by Developer on 12/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "CommonUtils.h"

@interface ProductDetailViewController () <UIScrollViewDelegate> {
    
    __weak IBOutlet UIScrollView *scroll;
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIButton *btnChat;
    __weak IBOutlet UIButton *btnGotoShop;
    __weak IBOutlet UIButton *btnAddCart;
    __weak IBOutlet UIButton *btnBuyNow;    
    __weak IBOutlet UILabel *lblDiv;
    
    
    CGFloat _scrollViewWidth;
    int _pageNumber;
    int imageCount;
    NSTimer *_timer;
}

@end

@implementation ProductDetailViewController
@synthesize _from;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self initView];
    
    _scrollViewWidth = 0;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(autoScroll)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];    
}

- (void) initView {
    
    if (_from == FROM_SHOP) {
        
        [btnChat setHidden:YES];
        [btnGotoShop setHidden:YES];
        [btnAddCart setHidden:YES];
        [btnBuyNow setHidden:YES];
        [lblDiv setHidden:YES];
    }
    
    self.tabBarController.tabBar.hidden = YES;
    
    imageCount = 3;
   
    // Scroll View
    
    scroll.backgroundColor=[UIColor clearColor];
    scroll.delegate=self;
    scroll.pagingEnabled=YES;
    scroll.alwaysBounceVertical = NO;
    scroll.bounces = NO;
    
    [scroll setContentSize:CGSizeMake(self.view.frame.size.width*imageCount, self.view.frame.size.height*0.38)];
    
    // page control
    
    pageControl.backgroundColor=[UIColor clearColor];
    pageControl.numberOfPages=imageCount;
    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    for(int i=1;i<=imageCount;i++)
    {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, self.view.frame.size.height*0.38)];
        [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"image%d",i]]];
        [scroll addSubview:image];
        
        x+=self.view.frame.size.width;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
   
    _scrollViewWidth = _scrollView.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    _pageNumber = floor((_scrollView.contentOffset.x - _scrollViewWidth/50) / _scrollViewWidth) +1;
    
    pageControl.currentPage=_pageNumber;
    
}

- (void)pageChanged {
    
    int pageNumber = (int)pageControl.currentPage;
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [scroll scrollRectToVisible:frame animated:YES];
}

- (void) autoScroll {
    
    pageControl.currentPage=_pageNumber++;
    
    if (_pageNumber >= imageCount) {
        
        _pageNumber = 0;
    }
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*_pageNumber;
    frame.origin.y=0;
    
    if (_pageNumber == 0) {
        
        [scroll scrollRectToVisible:frame animated:NO];
        
    } else {
    
        [scroll scrollRectToVisible:frame animated:YES];
    }
    
    
    
}
- (IBAction)addCartAction:(id)sender {
    
    [self.tabBarController setSelectedIndex:3];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    [_timer invalidate];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
