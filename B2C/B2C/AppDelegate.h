//
//  AppDelegate.h
//  B2C
//
//  Created by Developer on 12/13/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>



@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UserEntity *Me;

@end

