//
//  CountryEntity.m
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CountryEntity.h"

@implementation CountryEntity

@synthesize _countryName, _dialCode;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _countryName = @"";
        _dialCode = @"";
    }
    
    return self;
}


@end
