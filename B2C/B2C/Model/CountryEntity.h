//
//  CountryEntity.h
//  B2C
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryEntity : NSObject

@property (nonatomic, strong) NSString * _countryName;

@property (nonatomic, strong) NSString * _dialCode;

@end
